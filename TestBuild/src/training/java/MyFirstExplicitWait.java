import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by Koen on 4-2-2016.
 */
public class MyFirstExplicitWait {
    public static FirefoxDriver driver;

    @BeforeClass
    public static void createDriver()
    {
        driver = new FirefoxDriver();
    }

    @Test
    public void explicitWaitTest()
    {
        driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");

        WebDriverWait waitForStartButton = new WebDriverWait(driver, 2);

        waitForStartButton.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#start>button")));

        WebElement startbutton = driver.findElement(By.cssSelector("#start>button"));

        startbutton.click();

        WebDriverWait waitForHelloText = new WebDriverWait(driver,10);

        waitForHelloText.until(ExpectedConditions.visibilityOfElementLocated(By.id("finish")));

        WebElement helloWorldText = driver.findElement(By.id("finish"));

        String textFromElement = helloWorldText.getText();

        assertThat(textFromElement, equalTo("Hello World!"));
    }

    @AfterClass
    public static void quitDriver()
    {
        driver.quit();
    }
}
