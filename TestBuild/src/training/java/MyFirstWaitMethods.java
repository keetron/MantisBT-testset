import com.google.common.base.Function;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by Koen on 5-2-2016.
 */
public class MyFirstWaitMethods {
    public static FirefoxDriver driver;

    @BeforeClass
    public static void createDriver()
    {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void quitDriver()
    {
        driver.quit();
    }

    public void waitForElementToBeVisible(By selector, int timeToWaitInSeconds)
    {
        WebDriverWait wait = new WebDriverWait(driver, timeToWaitInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
    }

    public WebElement findWebElementWhileWaitingFluently(final By selector, int timeoutInSeconds)
    {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement webElement = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(selector);
            }
        });

        return webElement;
    }

    @Test
    public void clickStartButton_ExplicitWait()
    {
        driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
        waitForElementToBeVisible(By.cssSelector("#start>button"), 2);
        WebElement startButton = driver.findElement(By.cssSelector("#start>button"));
        startButton.click();
        waitForElementToBeVisible(By.id("finish"), 10);
        WebElement helloWorldText = driver.findElement(By.id("finish"));
        String textFromElement = helloWorldText.getText();
        assertThat(textFromElement, equalTo("Hello World!"));

    }

    @Test
    public void clickStartButton_FluentWait()
    {
        driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
        WebElement startButton = findWebElementWhileWaitingFluently(By.cssSelector("#start>button"), 2);
        startButton.click();
        WebElement helloWorldText = findWebElementWhileWaitingFluently(By.id("finish"), 10);
        String textFromElement = helloWorldText.getText();
        assertThat(textFromElement, equalTo("Hello World!"));

    }

}
