import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertTrue;


/**
 * Created by Koen on 3-2-2016.
 */
public class ChromeTest {

    @BeforeClass
    public static void setupChromeDriverLocation()
    {
        String chromeDriverLocation = "D:\\SeleniumDrivers\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
    }

    @Test
    public void basicChromeUsage()
    {
        WebDriver chrome = new ChromeDriver();
        chrome.get("http://localhost:8057/mantisbt/");
        assertTrue(chrome.getTitle().equals("MantisBT"));
        chrome.quit();
    }

}
