import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Koen on 4-2-2016.
 */
public class MyFirstAssertionTest {
    public static FirefoxDriver driver;

    @BeforeClass
    public static void setupDriver()
    {
        driver = new FirefoxDriver();
    }

    @Before
    public void goToAccountPage()
    {
        driver.get("http://store.demoqa.com/products-page/your-account/");
    }

    @Test
    public void assertTrue_PageTitle()
    {
        assertTrue("Error with page title on 'Your account' page",driver.getTitle().equals("Your Account | ONLINE STORE"));
    }

    @Test
    public void assertEquals_FormTitleIsYourAccount()
    {
        WebElement yourAccountLabel = driver.findElement(By.className("entry-title"));

        String yourAccountLabelText = yourAccountLabel.getText();

        assertEquals("Text in the 'Your Account' field was not as expected", yourAccountLabelText, "Your Account");

    }

    @Test
    public void assertThat_FooterContainsCertainText()
    {
        WebElement footerLaber = driver.findElement(By.cssSelector("#footer_nav>p"));

        String footerLabelText = footerLaber.getText();

        assertThat(footerLabelText, containsString("Splashing Pixels"));
    }



    @AfterClass
    public static void quitDriver()
    {
        driver.quit();
    }
}
