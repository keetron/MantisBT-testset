import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.linkText;

/**
 * Created by Koen on 5-2-2016.
 */
public class CreateIssue {
    public static FirefoxDriver driver;

    @BeforeClass
    public static void createDriver()
    {
        driver = new FirefoxDriver();
    }


     @Test
    public void CreateIssue(){
        driver.get("http://localhost:8057/mantisbt/");
        driver.findElement(By.name("username")).sendKeys("TestUser1");
        driver.findElement(By.name("password")).sendKeys("Test");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.linkText("Report Issue")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue and the text will be as generic as can be");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[4]/td[4]/a")).click();
        assertTrue("We did not end up on the issue page",driver.getPageSource().contains("View Issue Details"));
        driver.findElement(linkText("Basics.Logout")).click();
    }

    @AfterClass
    public static void quitDriver()
    {

        driver.quit();
    }


}
