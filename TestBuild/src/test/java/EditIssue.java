import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.linkText;

/**
 * Created by Koen on 12-2-2016.
 */
public class EditIssue {

    //This to easily capture and change URL
    //todo put in config file
    public static final String URL = Generics.BaseURL;
    public static FirefoxDriver driver;

    //much used variables
    private String bugnoteText;
    private String bodyText;

    @BeforeClass
    public static void createDriver() {

        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void quitDriver() {
        driver.quit();
    }


    @Test //completed
    public void EditIssueBaseFields() {
        //first create an issue to use for testing
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue for use with the EditIssueBaseFields test");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //click edit button on details screen and assert if we ended up on issue screen
        driver.findElement(By.cssSelector("td.center > form > input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("Updating Issue Information"));

        //edit base fields on detail screen
        //fields to edit: Reporter, Assigned To, Priority, Severity, Reproducibility, Status, Resolution, Platform, OS, OS Version
        new Select(driver.findElement(By.name("handler_id"))).selectByVisibleText("developer");
        new Select(driver.findElement(By.name("priority"))).selectByVisibleText("urgent");
        new Select(driver.findElement(By.name("severity"))).selectByVisibleText("crash");
        new Select(driver.findElement(By.name("reproducibility"))).selectByVisibleText("unable to reproduce");
        new Select(driver.findElement(By.name("status"))).selectByVisibleText("acknowledged");
        new Select(driver.findElement(By.name("resolution"))).selectByVisibleText("unable to reproduce");
        driver.findElement(By.name("platform")).clear();
        driver.findElement(By.id("platform")).sendKeys("Windows");
        driver.findElement(By.name("os")).clear();
        driver.findElement(By.id("os")).sendKeys("Win10");
        driver.findElement(By.name("os_build")).clear();
        driver.findElement(By.id("os_build")).sendKeys("10");
        driver.findElement(By.cssSelector("input.button")).click();

        //quick assert if we are on the right issue page
        String PageURL = driver.getCurrentUrl();
        Assert.assertEquals(PageURL, IssueURL);

        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("Assigned To developer"));
        Assert.assertTrue(bodyText.contains("Priority urgent"));
        Assert.assertTrue(bodyText.contains("Severity crash"));
        Assert.assertTrue(bodyText.contains("Reproducibility unable to reproduce"));
        Assert.assertTrue(bodyText.contains("Resolution unable to reproduce"));
        Assert.assertTrue(bodyText.contains("Platform Windows"));
        Assert.assertTrue(bodyText.contains("OS Win10"));
        Assert.assertTrue(bodyText.contains("OS Version 10"));

        driver.get(URL + "view_all_bug_page.php");
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("crash acknowledged (developer)"));

        //go to defect page to edit the remaining fields
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //click edit button on details screen and assert if we ended up on issue screen
        driver.findElement(By.cssSelector("td.center > form > input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue edit page", bodyText.contains("Updating Issue Information"));

        //fields to edit: Summary, Description, Steps To Reproduce, Additional Information, Add Note
        driver.findElement(By.name("summary")).clear();
        driver.findElement(By.name("summary")).sendKeys("This is a test issue and the text will be as generic as can be (processed) This test has been set up to test EditIssueBaseFields");
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description. We worked on this issue");
        driver.findElement(By.name("steps_to_reproduce")).clear();
        driver.findElement(By.name("steps_to_reproduce")).sendKeys("Cannot be reproduced");
        driver.findElement(By.name("additional_information")).clear();
        driver.findElement(By.name("additional_information")).sendKeys("Here comes the additional information");
        driver.findElement(By.name("bugnote_text")).sendKeys("Find an additional note here.");
        driver.findElement(By.cssSelector("input.button")).click();

        //quick assert if we are on the right issue page
        PageURL = driver.getCurrentUrl();
        Assert.assertEquals(PageURL, IssueURL);

        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("This is a test issue and the text will be as generic as can be (processed) This test has been set up to test EditIssueBaseFields"));
        Assert.assertTrue(bodyText.contains("Description This is the long text for the description. We worked on this issue"));
        Assert.assertTrue(bodyText.contains("Steps To Reproduce Cannot be reproduced"));
        Assert.assertTrue(bodyText.contains("Additional Information Here comes the additional information"));
        Assert.assertTrue(bodyText.contains("Find an additional note here."));

        driver.get(URL + "view_all_bug_page.php");
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("This is a test issue and the text will be as generic as can be (processed) This test has been set up to test EditIssueBaseFields"));

        //Basics.Logout
        driver.get(URL + "logout_page.php");
    }

    @Test //completed
    public void EditIssueAssignTo() {

        //first create an issue to use for testing
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue for use with the EditIssueAssignTo test");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //Assign to developer
        new Select(driver.findElement(By.name("handler_id"))).selectByVisibleText("developer");
        driver.findElement(By.xpath("//input[@value='Assign To:']")).click();

        //Issue should now be visible for developer in Assigned to section
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Check on assignee failed", bodyText.contains("Assigned To developer"));
        //check history for the change in assignee developer
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Check on history assignee part 1 failed", bodyText.contains("Assigned To => developer"));
        Assert.assertTrue("Check on history assignee part 2 failed", bodyText.contains("Status new => assigned"));

        //Basics.Logout
        driver.get(URL + "logout_page.php");
    }

    @Test //completed
    public void EditIssueChangeStatus() {
        //first create an issue to use for testing
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue for use with the EditIssueChangeStatus test");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //From here there is a series of status changes
        //Status change to Feedback
        new Select(driver.findElement(By.name("new_status"))).selectByVisibleText("feedback");
        driver.findElement(By.xpath("//input[@value='Change Status To:']")).click();
        bugnoteText = "This is a generic note for the change status to Feedback test.";
        driver.findElement(By.name("bugnote_text")).sendKeys(bugnoteText);
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("Status feedback"));
        Assert.assertTrue(bodyText.contains(bugnoteText));
        Assert.assertTrue("Check on history statuschange feedback failed", bodyText.contains("Status new => feedback"));

        //Status change to Acknowledged
        new Select(driver.findElement(By.name("new_status"))).selectByVisibleText("acknowledged");
        driver.findElement(By.xpath("//input[@value='Change Status To:']")).click();
        bugnoteText = "This is a generic note for the change status to Acknowledge test.";
        driver.findElement(By.name("bugnote_text")).sendKeys(bugnoteText);
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("Status acknowledged"));
        Assert.assertTrue(bodyText.contains(bugnoteText));
        Assert.assertTrue("Check on history statuschange acknowledged failed", bodyText.contains("Status feedback => acknowledged"));

        //Status change to Confirmed
        new Select(driver.findElement(By.name("new_status"))).selectByVisibleText("confirmed");
        driver.findElement(By.xpath("//input[@value='Change Status To:']")).click();
        bugnoteText = "This is a generic note for the Change Status to Confirmed test";
        driver.findElement(By.name("bugnote_text")).sendKeys(bugnoteText);
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("Status confirmed"));
        Assert.assertTrue(bodyText.contains(bugnoteText));
        Assert.assertTrue("Check on history statuschange confirmed failed", bodyText.contains("Status acknowledged => confirmed"));

        //Status change to Assigned
        new Select(driver.findElement(By.name("new_status"))).selectByVisibleText("assigned");
        driver.findElement(By.xpath("//input[@value='Change Status To:']")).click();
        bugnoteText = "This is a generic test to change the status to Assigned.";
        driver.findElement(By.name("bugnote_text")).sendKeys(bugnoteText);
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("Status assigned"));
        Assert.assertTrue(bodyText.contains(bugnoteText));
        Assert.assertTrue("Check on history statuschange assigned failed", bodyText.contains("Status confirmed => assigned"));

        //Status change to Closed
        new Select(driver.findElement(By.name("new_status"))).selectByVisibleText("closed");
        driver.findElement(By.xpath("//input[@value='Change Status To:']")).click();
        bugnoteText = "This is a generic comment to change the status to Closed";
        driver.findElement(By.name("bugnote_text")).sendKeys(bugnoteText);
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("Status closed"));
        Assert.assertTrue(bodyText.contains(bugnoteText));
        Assert.assertTrue("Check on history statuschange closed failed", bodyText.contains("Status assigned => closed"));

        //Basics.Logout
        driver.get(URL + "logout_page.php");

    }


    @Test //completed
    public void EditIssueMakeSticky() {
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue for use with the EditIssueMakeSticky test");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        driver.findElement(By.xpath("//input[@value='Stick']")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("History check on Stick Issue failed", bodyText.contains("Sticky Issue No => Yes"));
        driver.findElement(By.xpath("//input[@value='Unstick']")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("History check on Stick Issue failed", bodyText.contains("Sticky Issue Yes => No"));

        //Basics.Logout
        driver.get("http://localhost:8057/mantisbt/logout_page.php");
    }

    @Test //completed
    public void EditIssueClone() {
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue for use with the EditIssueClone test");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //Click Clone button
        driver.findElement(By.xpath("//input[@value='Clone']")).click();

        //change all details
        new Select(driver.findElement(By.name("reproducibility"))).selectByVisibleText("sometimes");
        new Select(driver.findElement(By.name("severity"))).selectByVisibleText("feature");
        new Select(driver.findElement(By.name("priority"))).selectByVisibleText("none");
        new Select(driver.findElement(By.name("handler_id"))).selectByVisibleText("manager");
        driver.findElement(By.name("summary")).clear();
        driver.findElement(By.name("summary")).sendKeys("This test is to check on a Cloned issue");
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys("This is the description for a Cloned issue");
        driver.findElement(By.name("steps_to_reproduce")).clear();
        driver.findElement(By.name("steps_to_reproduce")).sendKeys("Series of steps goes here.");
        driver.findElement(By.name("additional_info")).clear();
        driver.findElement(By.name("additional_info")).sendKeys("Here comes the additional information. As in all the issues.");
        driver.findElement(By.id("copy_notes_from_parent")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //check for new issue details in main and detail screen
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String ClonedIssueURL = driver.getCurrentUrl();

        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains("This test is to check on a Cloned issue"));
        Assert.assertTrue(bodyText.contains("Issue generated from:"));
        Assert.assertTrue(bodyText.contains("This is the description for a Cloned issue"));

        //Basics.Logout
        driver.get("http://localhost:8057/mantisbt/logout_page.php");
    }

    @Test //completed
    public void EditIssueMove() {
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue for use with the EditIssueMove test");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //create project to use in Move Test
        //goto project page
        driver.get(URL + "manage_proj_page.php");

        //check if there is already a project with name MoveIssueTestProject
        bodyText = driver.findElement(By.tagName("body")).getText();
        String projectName = "MoveIssueTestProject";
        Boolean projectNameExists = bodyText.contains(projectName);

        //if not, create the project with name MoveIssueTestProject
        if (!projectNameExists) {
            driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
            driver.findElement(By.name("name")).clear();
            driver.findElement(By.name("name")).sendKeys(projectName);
            driver.findElement(By.name("description")).clear();
            driver.findElement(By.name("description")).sendKeys("This project is in place to test if an issue can be moved from one project to another.");
            driver.findElement(By.cssSelector("input.button")).click();
        }

        //open issue for this test and move to MoveIssueTestProject
        driver.get(IssueURL);
        driver.findElement(By.xpath("//input[@value='Move']")).click();
        new Select(driver.findElement(By.xpath("(//select[@name='project_id'])[2]"))).selectByVisibleText(projectName);
        driver.findElement(By.cssSelector("input.button")).click();

        //check if the issue now in project MoveIssueTestProject
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("project name cannot be found", bodyText.contains(projectName));
        Assert.assertTrue("history check on project move", bodyText.contains("Project Test Project Start => MoveIssueTestProject"));

        //Basics.Logout
        driver.get("http://localhost:8057/mantisbt/logout_page.php");
    }

    @Test //complete
    public void EditIssueClose() {
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue for use with the EditIssueClose test");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //click close button, close issue
        driver.findElement(By.xpath("//input[@value='Close']")).click();
        new Select(driver.findElement(By.name("resolution"))).selectByVisibleText("won't fix");
        driver.findElement(By.name("bugnote_text")).clear();
        driver.findElement(By.name("bugnote_text")).sendKeys("This is a note for the EditIssueClose test");
        driver.findElement(By.cssSelector("input.button")).click();

        //check for closed status
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Status closed check failed", bodyText.contains("Status closed"));

        //Reopen
        driver.findElement(By.xpath("//input[@value='Reopen']")).click();
        driver.findElement(By.name("bugnote_text")).clear();
        driver.findElement(By.name("bugnote_text")).sendKeys("This is the reopen text for EditIssueClose");
        driver.findElement(By.cssSelector("input.button")).click();

        //check for feedback status and history check
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Status feedback check failed", bodyText.contains("Status feedback"));
        Assert.assertTrue("history check failed", bodyText.contains("Resolution open => won't fix"));
        Assert.assertTrue("history feedback check failed", bodyText.contains("Status new => closed"));
        Assert.assertTrue("history feedback check failed", bodyText.contains("Status closed => feedback"));
        Assert.assertTrue("history feedback check failed", bodyText.contains("Resolution won't fix => reopened"));

        //Basics.Logout
        driver.get(URL + "logout_page.php");
    }

    @Test //complete
    public void EditIssueDelete() {
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        driver.findElement(By.name("summary")).sendKeys("This is a test issue for use with the EditIssueDelete test");
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //click delete, confirm delete
        driver.findElement(By.xpath("//input[@value='Delete']")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //check deleted status
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("This issue is normally in place", bodyText.contains("APPLICATION ERROR #1100"));

        //Basics.Logout
        driver.get(URL + "logout_page.php");
    }

    @Test //completed
    public void EditIssueCreateRelationship() {
        //create 10 issues to use in this test, record issue numbers and put in array.
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();

        //create 10 new issues to use in pairs for the various relationships
        String[] issueID = new String[10];
        int issueCount = 10;
        for (int i = 0; i < issueCount; i++) {
            //create issues as long as we created fewer than 10 issues
            driver.get(URL + "bug_report_page.php");
            Select dropdown = new Select(driver.findElement(By.name("category_id")));
            dropdown.selectByIndex(1);
            driver.findElement(By.name("summary")).sendKeys("This is the test issue with ArrayID " + i + " for use with the EditIssueCreateRelationship test");
            driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
            driver.findElement(By.cssSelector("input.button")).click();
            driver.findElement(By.partialLinkText("View Submitted Issue")).click();
            assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
            String IssueURL = driver.getCurrentUrl();

            //retrieve issueID from URL and put in issueID[]
            issueID[i] = IssueURL.replace(URL + "view.php?id=", "");
        }
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //now that we have created 10 issues, pair up 0&1, 2&3, 4&5, 6&7 and 8&9 with separate relationships
        //link 0&1 as a standard relationship
        driver.get(URL + "view.php?id=" + issueID[0]);
        driver.findElement(By.name("dest_bug_id")).clear();
        driver.findElement(By.name("dest_bug_id")).sendKeys(issueID[1]);
        driver.findElement(By.name("add_relationship")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("relationship listed in detailview", bodyText.contains("related to 0000"));
        Assert.assertTrue("historycheck on relationship status between 0&1 failed", bodyText.contains("Relationship added related to"));

        //link 2&3 with 2 as a parent for 3 (she was as a mother to her)
        driver.get(URL + "view.php?id=" + issueID[2]);
        driver.findElement(By.name("dest_bug_id")).clear();
        driver.findElement(By.name("dest_bug_id")).sendKeys(issueID[3]);
        new Select(driver.findElement(By.name("rel_type"))).selectByVisibleText("parent of");
        driver.findElement(By.name("add_relationship")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("relationship listed in detailview", bodyText.contains("parent of 0000"));
        Assert.assertTrue("historycheck on relationship status between 2&3 failed", bodyText.contains("Relationship added parent of"));

        //link 4&5 with 4 as a child for 5 (adopted, obviously)
        driver.get(URL + "view.php?id=" + issueID[4]);
        driver.findElement(By.name("dest_bug_id")).clear();
        driver.findElement(By.name("dest_bug_id")).sendKeys(issueID[5]);
        new Select(driver.findElement(By.name("rel_type"))).selectByVisibleText("child of");
        driver.findElement(By.name("add_relationship")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("relationship listed in detailview", bodyText.contains("child of 0000"));
        Assert.assertTrue("historycheck on relationship status between 4&5 failed", bodyText.contains("Relationship added child of"));

        //link 6&7 with 6 as a duplicate to 7 (it's complicated)
        driver.get(URL + "view.php?id=" + issueID[6]);
        driver.findElement(By.name("dest_bug_id")).clear();
        driver.findElement(By.name("dest_bug_id")).sendKeys(issueID[7]);
        new Select(driver.findElement(By.name("rel_type"))).selectByVisibleText("duplicate of");
        driver.findElement(By.name("add_relationship")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("relationship listed in detailview", bodyText.contains("duplicate of 0000"));
        Assert.assertTrue("historycheck on relationship status between 6&7 failed", bodyText.contains("Relationship added duplicate of"));

        //link 8&9 with 8 who has 9 as a duplicate (it is more complicated)
        driver.get(URL + "view.php?id=" + issueID[8]);
        driver.findElement(By.name("dest_bug_id")).clear();
        driver.findElement(By.name("dest_bug_id")).sendKeys(issueID[9]);
        new Select(driver.findElement(By.name("rel_type"))).selectByVisibleText("has duplicate");
        driver.findElement(By.name("add_relationship")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("relationship listed in detailview", bodyText.contains("has duplicate 0000"));
        Assert.assertTrue("historycheck on relationship status between 8&9 failed", bodyText.contains("Relationship added has duplicate"));

        //Basics.Logout
        driver.get(URL + "logout_page.php");
    }
/*
        //I have no idea how to test this
        //todo figure out how to test this

    @Test
    public void EditIssueUploadFile(){
        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[4]/td[4]/a")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));

        //Select File

        //upload file

        //check in history


        //Basics.Logout
        driver.get("http://localhost:8057/mantisbt/logout_page.php");
    }
*/

    @Test //complete
    public void EditIssueMonitor() {
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        String summaryText = "This is a test issue for use with the EditIssueMonitor test attempt 45";
        driver.findElement(By.name("summary")).sendKeys(summaryText);
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //click Monitor
        driver.findElement(By.xpath("//input[@value='Monitor']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("historycheck administrator", bodyText.contains("Issue Monitored: administrator"));

        driver.findElement(By.linkText("My View")).click();
        driver.findElement(By.linkText("Monitored by Me")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("summary visible in Monitored by me", bodyText.contains(summaryText));

        //add user name to monitor (username: developer)
        driver.get(IssueURL);
        driver.findElement(By.name("username")).sendKeys("developer");
        driver.findElement(By.xpath("(//input[@value='Add'])[2]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("monitorcheck", bodyText.contains("administrator [Delete], developer [Delete]"));
        Assert.assertTrue("historycheck developer", bodyText.contains("Issue Monitored: developer"));

        //use End monitoring to end monitoring for administrator
        driver.findElement(By.xpath("//input[@value='End Monitoring']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("monitorcheck administrator delete", bodyText.contains("administrator [Delete]"));
        Assert.assertTrue("historycheck end administrator", bodyText.contains("Issue End Monitor: administrator"));

        //remove developer from monitoring
        driver.findElement(By.linkText("Delete")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("monitorcheck developer delete", bodyText.contains("developer [Delete]"));
        Assert.assertTrue("historycheck end developer", bodyText.contains("Issue End Monitor: developer"));

        //Basics.Logout
        driver.get(URL + "logout_page.php");
    }


    @Test
    public void EditIssueAddPublicNote() {
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        String summaryText = "This is a test issue for use with the EditIssueAddPublicNote test";
        driver.findElement(By.name("summary")).sendKeys(summaryText);
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //type some note, click add note
        bugnoteText = "This is an issuenote, added specifically for the add note test. This is a public note.";
        driver.findElement(By.name("bugnote_text")).sendKeys(bugnoteText);
        driver.findElement(By.cssSelector("td.center > input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("the note was note found", bodyText.contains(bugnoteText));

        //edit note
        bugnoteText = "This is an issuenote, added specifically for the add note test. This is a public note. Now this text is here for an edit.";
        driver.findElement(By.cssSelector("div.small > form > input.button-small")).click();
        driver.findElement(By.name("bugnote_text")).clear();
        driver.findElement(By.name("bugnote_text")).sendKeys(bugnoteText);
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("the note was note found", bodyText.contains(bugnoteText));

        //make note private
        driver.findElement(By.xpath("//input[@value='Make Private']")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("the note was note found", bodyText.contains("administrator (administrator) [ private ]"));

        //delete note
        driver.findElement(By.xpath("(//input[@value='Delete'])[2]")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("the note was found", bodyText.contains(bugnoteText));

        //Basics.Logout
        driver.get(URL + "logout_page.php");
    }


    @Test //completed
    public void EditIssueAddPrivateNote() {
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        String summaryText = "This is a test issue for use with the EditIssueAddPublicNote test";
        driver.findElement(By.name("summary")).sendKeys(summaryText);
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        assertTrue("We did not end up on the issue page", bodyText.contains(summaryText));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains(summaryText));

        //type some note, click checkbox private, click add note
        bugnoteText = "This is an issuenote, added specifically for the add note test. This is a private note. Now this text is here for an edit.";
        driver.findElement(By.name("bugnote_text")).clear();
        driver.findElement(By.name("bugnote_text")).sendKeys(bugnoteText);
        driver.findElement(By.name("private")).click();
        driver.findElement(By.cssSelector("td.center > input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("the note was note found", bodyText.contains("administrator (administrator) [ private ]"));
        Assert.assertTrue("the note was note found", bodyText.contains(bugnoteText));

        //make public
        driver.findElement(By.xpath("//input[@value='Make Public']")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("the note was found to be private", bodyText.contains("administrator (administrator) [ private ]"));

        //delete note
        driver.findElement(By.xpath("(//input[@value='Delete'])[2]")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("the note was found", bodyText.contains(bugnoteText));

        //Basics.Logout
        driver.get(URL + "logout_page.php");
    }


    @Test //completed
    public void EditIssueResolvedResolution() {
        //create 10 issues to use in this test, record issue numbers and put in array.
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();

        //Array containing all 9 resolution types
        String[] resolutionType = {"open", "fixed", "reopened", "unable to reproduce", "not fixable", "duplicate", "no change required", "suspended", "won't fix"};

        //create 9 new issues to use for the resolution types
        String[] issueID = new String[9];
        int issueCount = 9;
        for (int i = 0; i < issueCount; i++) {
            //create issues as long as we created fewer than 10 issues
            driver.get(URL + "bug_report_page.php");
            Select dropdown = new Select(driver.findElement(By.name("category_id")));
            dropdown.selectByIndex(1);
            driver.findElement(By.name("summary")).sendKeys("This is the test issue with ArrayID " + i + " for use with the EditIssueResolvedResolution test and the resolution " + resolutionType[i]);
            driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
            driver.findElement(By.cssSelector("input.button")).click();
            driver.findElement(By.partialLinkText("View Submitted Issue")).click();
            assertTrue("We did not end up on the issue page", driver.getPageSource().contains("View Issue Details"));
            String IssueURL = driver.getCurrentUrl();

            //retrieve issueID from URL and put in issueID[]
            issueID[i] = IssueURL.replace(URL + "view.php?id=", "");
        }
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue page and change state of the issue to resolved with the various resolution type
        for (int i = 0; i < issueCount; i++) {
            driver.get(URL + "view.php?id=" + issueID[i]);
            new Select(driver.findElement(By.name("new_status"))).selectByVisibleText("resolved");
            driver.findElement(By.xpath("//input[@value='Change Status To:']")).click();
            new Select(driver.findElement(By.name("resolution"))).selectByVisibleText(resolutionType[i]);
            driver.findElement(By.cssSelector("input.button")).click();
            bodyText = driver.findElement(By.tagName("body")).getText();
            Assert.assertTrue("the resolution is not stored properly", bodyText.contains("Status resolved Resolution " + resolutionType[i]));
            Assert.assertTrue("history check resolution", bodyText.contains("Status new => resolved"));

        }

        //Basics.Logout
        driver.get("http://localhost:8057/mantisbt/logout_page.php");

    }


}
