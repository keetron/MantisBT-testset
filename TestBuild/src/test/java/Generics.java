
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.io.*;

import static org.junit.Assert.assertEquals;


/**
 * Created by Koen on 5-2-2016.
 */
public class Generics {

    public static FirefoxDriver driver;
    public static final String BaseURL = "http://localhost:8057/mantisbt/";
    String URL = BaseURL;


    public static void createDriver()
    {
        driver = new FirefoxDriver();
    }

   public void Login(String username, String password)
    {
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys(username);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.cssSelector("input.button")).click();
    }

}

