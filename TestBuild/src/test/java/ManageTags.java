import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.linkText;

/**
 * Created by Koen on 24-2-2016.
 */
public class ManageTags {
    // todo move to config file
    public static final String URL = "http://localhost:8057/mantisbt/";
    public static FirefoxDriver driver;

    @BeforeClass
    public static void createDriver() {

        driver = new FirefoxDriver();
    }

    @Test //completed
    public void CreateTag(){
        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage Tags page
        driver.get(URL + "manage_tags_page.php");
        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("Manage Tags"));
        Assert.assertTrue("The header is matching the page", driver.getTitle().contains("Manage Tags"));

        //add tag
        String TagName = "Tagtest";
        String TagDescription = "This is to describe the tag";
        driver.findElement(By.name("name")).sendKeys(TagName);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(TagDescription);
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("The Tag name is correct", bodyText.contains(TagName));

        //add existing Tag (basically nothing happens, this is matching behavior when adding an existing tag to an issue)
        driver.findElement(By.name("name")).sendKeys(TagName);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(TagDescription);
        driver.findElement(By.cssSelector("input.button")).click();
        Assert.assertTrue("The Tag name is correct", bodyText.contains(TagName));

        //open tag
        driver.findElement(By.linkText(TagName)).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("The Tag name is correct", bodyText.contains(TagName));
        Assert.assertTrue("The Tag description is present", bodyText.contains("Tag Description " + TagDescription));

        //Delete tag
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");

    }

    @Test //completed
    public void UpdateTag(){
        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage Tags page
        driver.get(URL + "manage_tags_page.php");
        String PageTitle = driver.getTitle();
        Assert.assertTrue("The header is matching the page", PageTitle.contains("Manage Tags"));

        //create tag to use in test
        String TagName = "TagtestUpdate";
        String TagDescription = "This is to describe the tag Update test";
        driver.findElement(By.name("name")).sendKeys(TagName);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(TagDescription);
        driver.findElement(By.cssSelector("input.button")).click();
        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("The Tag name is correct", bodyText.contains(TagName));

        //open tag
        driver.findElement(By.linkText(TagName)).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("The Tag name is correct", bodyText.contains(TagName));
        Assert.assertTrue("The Tag description is present", bodyText.contains("Tag Description " + TagDescription));

        //update tag
        String UpdatedTagName = "TagUpdatedName";
        String UpdatedTagDescription = "This is an updated tag description";
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(UpdatedTagName);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(UpdatedTagDescription);
        driver.findElement(By.cssSelector("input.button")).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("The updated tag name is correct", bodyText.contains(UpdatedTagName));
        Assert.assertTrue("The updated tag description is present", bodyText.contains("Tag Description " + UpdatedTagDescription));

        //Delete tag
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");

    }

    @Test //completed
    public void CreateTagFromIssuePage(){
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        String summaryText = "This is a test issue for use with the EditIssueAddPublicNote test";
        driver.findElement(By.name("summary")).sendKeys(summaryText);
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page",driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //add tag in issue
        String IssueTag = "IssueTag";
        driver.findElement(By.id("tag_string")).clear();
        driver.findElement(By.id("tag_string")).sendKeys(IssueTag);
        driver.findElement(By.cssSelector("input.button")).click();

        //check tag in issue
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Tag check in issue", bodyText.contains(IssueTag));

        //check tag on tags page
        driver.get(URL + "manage_tags_page.php");
        String PageTitle = driver.getTitle();
        Assert.assertTrue("The header is matching the page", PageTitle.contains("Manage Tags"));
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Tag check on Issuepage", bodyText.contains(IssueTag));

        //go back to issue
        driver.get(IssueURL);

        //remove tag from issue
        driver.findElement(By.cssSelector("img.delete-icon")).click();

        //check tag on issue page
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Tag check on Issuepage", bodyText.contains("No tags attached."));

        //Delete tag
        driver.get(URL + "manage_tags_page.php");
        driver.findElement(By.linkText(IssueTag)).click();
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");

    }

    @Test //completed
    public void CreateTagCreateMultipleTags(){
        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage Tags page
        driver.get(URL + "manage_tags_page.php");
        String PageTitle = driver.getTitle();
        Assert.assertTrue("The header is matching the page", PageTitle.contains("Manage Tags"));

        //create multiple tags
        String[] MultiTagTest = {"MultiTag1", "MultiTag2"};
        driver.findElement(By.name("name")).sendKeys(MultiTagTest[0] + ", " + MultiTagTest[1]);
        driver.findElement(By.cssSelector("input.button")).click();

        //check tag page
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("The header is matching the page", bodyText.contains(MultiTagTest[0] + " administrator"));
        Assert.assertTrue("The header is matching the page", bodyText.contains(MultiTagTest[1] + " administrator"));

        //delete tags
        driver.findElement(By.linkText(MultiTagTest[0])).click();
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText(MultiTagTest[1])).click();
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");

    }

    @Test //completed
    public void CreateMultipleTagsFromIssuePage(){
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        String summaryText = "This is a test issue for use with the EditIssueAddPublicNote test";
        driver.findElement(By.name("summary")).sendKeys(summaryText);
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page",driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //add tags in issue
        String[] IssueTag = {"MultiIssueTag1", "MultiIssueTag2"};
        driver.findElement(By.id("tag_string")).clear();
        driver.findElement(By.id("tag_string")).sendKeys(IssueTag[0] + ", " + IssueTag[1]);
        driver.findElement(By.cssSelector("input.button")).click();

        //check tags in issue
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("Tags " + IssueTag[0]));

        //remove tags from issue
        driver.findElement(By.cssSelector("img.delete-icon")).click();
        driver.findElement(By.cssSelector("img.delete-icon")).click();

        //check tags on issue page
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("We did not end up on the issue page", bodyText.contains("Tags " + IssueTag[0]));

        //goto tag page and Delete tag
        driver.get(URL + "manage_tags_page.php");
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("The header is matching the page", bodyText.contains(IssueTag[0] + " administrator"));
        Assert.assertTrue("The header is matching the page", bodyText.contains(IssueTag[1] + " administrator"));

        //delete tags
        driver.findElement(By.linkText(IssueTag[0])).click();
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText(IssueTag[1])).click();
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");
    }

    @Test //completed
    public void AddTagsFromDropDown(){
        //create issue to use in this test
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("reporter");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Report Issue")).click();
        Select dropdown = new Select(driver.findElement(By.name("category_id")));
        dropdown.selectByIndex(1);
        String summaryText = "This is a test issue for use with the EditIssueAddPublicNote test";
        driver.findElement(By.name("summary")).sendKeys(summaryText);
        driver.findElement(By.name("description")).sendKeys("This is the long text for the description");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.partialLinkText("View Submitted Issue")).click();
        assertTrue("We did not end up on the issue page",driver.getPageSource().contains("View Issue Details"));
        String IssueURL = driver.getCurrentUrl();
        driver.findElement(linkText("Basics.Logout")).click();

        //login as Admin and end up on issue overview page
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to tag page and create tag for dropdown
        driver.get(URL + "manage_tags_page.php");
        String[] dropDownTag = {"DropdownTag1", "DropdownTag2"};
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(dropDownTag[0]);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(dropDownTag[1]);
        driver.findElement(By.cssSelector("input.button")).click();
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Tag not present", bodyText.contains(dropDownTag[0] + " administrator"));
        Assert.assertTrue("Tag not present", bodyText.contains(dropDownTag[1] + " administrator"));

        //open issue and check if we are on the issue page
        driver.get(IssueURL);
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("We did not end up on the issue page", bodyText.contains("View Issue Details"));

        //add tags in issue from dropdown
        new Select(driver.findElement(By.id("tag_select"))).selectByVisibleText(dropDownTag[0]);
        driver.findElement(By.cssSelector("input.button")).click();
        new Select(driver.findElement(By.id("tag_select"))).selectByVisibleText(dropDownTag[1]);
        driver.findElement(By.cssSelector("input.button")).click();

        //check tags on issue page
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Check Tag presence on Issue page", bodyText.contains("Tags " + dropDownTag[0]));

        //remove tags from issue
        driver.findElement(By.xpath("(//img[@alt='X'])[2]")).click();
        driver.findElement(By.cssSelector("img.delete-icon")).click();

        //check tags on issue page
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Check Tag presence on Issue page", bodyText.contains("Tags " + dropDownTag[0]));

        //goto tag page and Delete tag
        driver.get(URL + "manage_tags_page.php");
        driver.findElement(By.linkText("DropdownTag1")).click();
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("DropdownTag2")).click();
        driver.findElement(By.xpath("//input[@value='Delete Tag']")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");
    }

    @After
    public void LogOut(){
        driver.get(URL + "logout_page.php");
    }

    @AfterClass
    public static void quitDriver()    {
        driver.quit();
    }


}
