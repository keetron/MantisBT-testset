import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Created by Koen on 9-2-2016.
 */
public class UserSetup {

    //This to easily capture and change URL
    //todo put in config file
    public static final String URL = "http://localhost:8057/mantisbt/";

    public static FirefoxDriver driver;

    //todo move this to config file? Rather not, it is not like accesslevel changes that much that reuse in other tests makes sense. A bit.
    public static final String[] accessLevel = {"viewer", "reporter", "updater", "developer", "manager"};

    @BeforeClass
    public static void createDriver()
    {
        driver = new FirefoxDriver();
    }

    @Test
    public void CreateUsers()
    {
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //for loop from here to insert all users from the array
        int size = accessLevel.length;
        for (int i=0; i<size; i++) {

            driver.findElement(By.linkText("Manage")).click();
            driver.findElement(By.linkText("Manage Users")).click();
            driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
            driver.findElement(By.name("username")).sendKeys(accessLevel[i]);
            driver.findElement(By.name("realname")).sendKeys(accessLevel[i]);
            driver.findElement(By.name("email")).sendKeys("test@test.com");
            new Select(driver.findElement(By.name("access_level"))).selectByVisibleText(accessLevel[i]);
            driver.findElement(By.cssSelector("input.button")).click();
            ;

            // Normally a file will be written in /xampp/mailoutput/ folder
            // This txt file contains the activation link
            // To simplify this test for now, email notifications are off, blank passwords are allowed, using $g_enable_email_notification in config_inc.php
            // A warning screen will follow asking for an ok on the blank password
            //todo read out activation mail from file written in mailoutput folder

            String bodyText = driver.findElement(By.tagName("body")).getText();
            Assert.assertTrue("Text not found!", bodyText.contains("The user has an empty password."));

            //Now just accept that the password is empty
            driver.findElement(By.cssSelector("input.button")).click();
            //Wait for the splash page goes away
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
/*
          //add user to the default project
          //user is already added to default project, it is a default project
          //todo include secondary projects to add users to
            new Select(driver.findElement(By.name("project_id[]"))).selectByVisibleText("Test Project Start");
            driver.findElement(By.xpath("//input[@value='Add User']")).click();
            String bodyTextProjectCheck = driver.findElement(By.tagName("body")).getText();
            Assert.assertTrue("Username not found", bodyTextProjectCheck.contains("Test Project Start ["));
*/

        }

        //logout to test the new users
        driver.get("http://localhost:8057/mantisbt/logout_page.php");
        driver.get(URL);

        //loop to test all users
        for (int i=0; i<size; i++) {

            driver.get(URL);
            //login with new user
            driver.findElement(By.name("username")).sendKeys(accessLevel[i]);
            // driver.findElement(By.id("password")).sendKeys("Test");
            // password is not needed (yet)
            //todo include password activation, see up for same comment
            driver.findElement(By.cssSelector("input.button")).click();

            //Assert this is the issue overview page for user "username"
            String bodyTextUsernameCheck = driver.findElement(By.tagName("body")).getText();
            Assert.assertTrue("Username not found", bodyTextUsernameCheck.contains(accessLevel[i]));
            driver.get("http://localhost:8057/mantisbt/logout_page.php");

        }
    }

    @AfterClass
    public static void quitDriver()
    {
        driver.quit();
    }


}
