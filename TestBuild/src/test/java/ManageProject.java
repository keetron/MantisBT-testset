import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.linkText;

/**
 * Created by keetr on 23-2-2016.
 */
public class ManageProject {

    // todo move to config file
    public static final String URL = "http://localhost:8057/mantisbt/";
    public static FirefoxDriver driver;

    @BeforeClass
    public static void createDriver() {

        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void quitDriver() {
        driver.quit();
    }

    @Test //completed
    public void createNewProject() {
        //login as admin
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to project screen
        driver.get(URL + "manage_proj_page.php");
        Assert.assertTrue(driver.getTitle().contains("Manage Projects - MantisBT"));

        //click button create new project
        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        String newProjectName = "CreateNewProject Project";
        driver.findElement(By.name("name")).sendKeys(newProjectName);
        String summaryText = "This project is created to allow the create new project test to run.";
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(summaryText);
        driver.findElement(By.cssSelector("input.button")).click();
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        if (bodyText.contains("APPLICATION ERROR #701")) {
            driver.get(URL + "manage_proj_page.php"); //goto main project page
            driver.findElement(By.linkText(newProjectName)).click(); //select the project that is already there for some reason
            driver.findElement(By.cssSelector("form > input.button")).click(); //Delete that mofo
            driver.findElement(By.cssSelector("input.button")).click(); //of course I am sure
            driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click(); //new project button, repeating the above
            driver.findElement(By.name("name")).clear();
            driver.findElement(By.name("name")).sendKeys(newProjectName);
            driver.findElement(By.name("description")).clear();
            driver.findElement(By.name("description")).sendKeys(summaryText);
            driver.findElement(By.cssSelector("input.button")).click();
        }

        driver.findElement(By.linkText("Proceed")).click(); //proceed on splash screen
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains projectname", bodyText.contains(newProjectName));
        Assert.assertTrue("Project overview page contains summary for project", bodyText.contains(summaryText));

        //check on project
        driver.findElement(By.linkText(newProjectName)).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project detail and edit page", bodyText.contains("Edit Project"));
        Assert.assertTrue("Project overview page contains projectname", bodyText.contains(newProjectName));
        Assert.assertTrue("Project overview page contains summary for project", bodyText.contains(summaryText));

        //delete project to clean up a bit
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.findElement(linkText("Basics.Logout")).click();

    }


    @Test //completed
    public void GlobalCategories() {
        //login as admin
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage projects screen
        driver.get(URL + "manage_proj_page.php");
        Assert.assertTrue(driver.getTitle().contains("Manage Projects - MantisBT"));

        //add global category
        driver.findElement(By.name("name")).sendKeys("TestCategory");
        driver.findElement(By.cssSelector("input.button")).click();


        //edit global category
        driver.findElement(By.cssSelector("tr.row-2 > td.center > form > input.button-small")).click();
        new Select(driver.findElement(By.name("assigned_to"))).selectByVisibleText("manager");
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();


        //Delete global category
        driver.findElement(By.xpath("(//input[@value='Delete'])[2]")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //logout
        driver.findElement(linkText("Basics.Logout")).click();
    }


    @Test //completed
    public void editProjectEditProject() {
        //login as admin
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage projects screen
        driver.get(URL + "manage_proj_page.php");
        Assert.assertTrue(driver.getTitle().contains("Manage Projects - MantisBT"));

        //create project to edit
        String EditProjectName = "EditProject Project";
        String EditProjectDescription = "This project is to test editing projects";
        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(EditProjectName);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(EditProjectDescription);
        driver.findElement(By.cssSelector("input.button")).click();

        //Proceed on splash screen
        driver.findElement(By.linkText("Proceed")).click();

        //Assert project in main project screen
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains project name", bodyText.contains(EditProjectName));
        Assert.assertTrue("Project overview page contains summary for project", bodyText.contains(EditProjectDescription));
        Assert.assertTrue("Project overview page contains project settings", bodyText.contains("development X public"));

        //open project and edit
        String EditProjectNameUpdated = "EditProject Updated Project";
        String EditProjectDescriptionUpdated = "This project is again to test editing projects. Now with updated text.";
        driver.findElement(By.linkText(EditProjectName)).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(EditProjectNameUpdated);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(EditProjectDescriptionUpdated);
        new Select(driver.findElement(By.name("status"))).selectByVisibleText("release");
        driver.findElement(By.name("enabled")).click();
        new Select(driver.findElement(By.name("view_state"))).selectByVisibleText("private");
        driver.findElement(By.cssSelector("input.button")).click();

        //assert changes
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Project overview page does not contain project name", bodyText.contains(EditProjectName));
        Assert.assertFalse("Project overview page does not contain summary for project", bodyText.contains(EditProjectDescription));
        Assert.assertTrue("Project overview page contains project name", bodyText.contains(EditProjectNameUpdated));
        Assert.assertTrue("Project overview page contains summary for project", bodyText.contains(EditProjectDescriptionUpdated));
        Assert.assertTrue("Project overview page contains project settings", bodyText.contains("release"));
        Assert.assertTrue("Project overview page contains project settings", bodyText.contains("private"));

        //delete project to clean up
        driver.findElement(By.linkText(EditProjectNameUpdated)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.findElement(linkText("Basics.Logout")).click();

    }

    @Test
    public void editProjectSubprojects() {
        //login as admin
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage projects screen
        driver.get(URL + "manage_proj_page.php");
        Assert.assertTrue(driver.getTitle().contains("Manage Projects - MantisBT"));

        //create sub and master projects
        String FirstSubProject = "SubProject";
        String FirstSubProjectdescription = "The First project to be used as a Subproject to the master";
        String SecondSubProject = "SubProject2";
        String SecondSubProjectdesciption = "The second project to use as a subproject to the master";
        String MasterProject = "Master Project";
        String MasterProjectdescription = "The Master project.";

        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(FirstSubProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(FirstSubProjectdescription);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();
        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(MasterProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(MasterProjectdescription);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert the two projects are created
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains first subproject", bodyText.contains(FirstSubProject));
        Assert.assertTrue("Project overview page contains masterproject", bodyText.contains(MasterProject));


        //Open Masterproject and add a new Subproject direct from this page
        driver.findElement(By.linkText(MasterProject)).click();
        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(SecondSubProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(SecondSubProjectdesciption);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert the Second subproject is added as a sub
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project is added as a sub", bodyText.contains("» " + SecondSubProject));
        //ASsert both other projects are still visible
        Assert.assertTrue("Project overview page contains first subproject", bodyText.contains(FirstSubProject));
        Assert.assertTrue("Project overview page contains masterproject", bodyText.contains(MasterProject));

        //update subproject inheritance
        //not sure how to do this at this time
        //todo figure out what this is and how it works and how to test it

        //open Master project and add subproject from dropdown
        driver.findElement(By.linkText(MasterProject)).click();
        new Select(driver.findElement(By.name("subproject_id"))).selectByVisibleText(FirstSubProject);
        driver.findElement(By.cssSelector("td > form > input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert the First subproject is added as a sub
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project is added as a sub", bodyText.contains("» " + FirstSubProject));

        //assert the Masterproject and second subproject are still visible
        Assert.assertTrue("Project overview page contains masterproject", bodyText.contains(MasterProject));
        Assert.assertTrue("Project is added as a sub", bodyText.contains("» " + SecondSubProject));

        //remove subprojects
        driver.findElement(By.linkText("Unlink")).click();
        driver.findElement(By.linkText("Proceed")).click();
        driver.findElement(By.linkText("Unlink")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert the subproject are no longer subs
        Assert.assertFalse("Project is no longer added as a sub", bodyText.contains("» " + FirstSubProject));
        Assert.assertFalse("Project is no longer added as a sub", bodyText.contains("» " + SecondSubProject));

        //assert the subprojects are still visible in the main page
        Assert.assertTrue("Project overview page contains first subproject", bodyText.contains(FirstSubProject));
        Assert.assertTrue("Project overview page contains subproject", bodyText.contains(SecondSubProject));

        //delete test projects
        driver.findElement(By.linkText(FirstSubProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText(SecondSubProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText(MasterProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //check if they are deleted properly
        Assert.assertFalse("Project is no longer visible", bodyText.contains(FirstSubProject));
        Assert.assertFalse("Project is no longer visible", bodyText.contains(SecondSubProject));
        Assert.assertFalse("Project is no longer visible", bodyText.contains(MasterProject));

        //logout
        driver.findElement(linkText("Basics.Logout")).click();

    }

    @Test //completed
    public void editProjectCategories() {
        //login as admin
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage projects screen
        driver.get(URL + "manage_proj_page.php");
        Assert.assertTrue(driver.getTitle().contains("Manage Projects - MantisBT"));

        //create two projects to edit
        String MasterProject = "zzFirst Edit Project";
        String MasterProjectdescription = "Project for editing the project";
        String SecondProject = "zzSecond Edit Project";
        String SecondProjectdescription = "Project for editing the category project";

        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(MasterProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(MasterProjectdescription);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(SecondProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(SecondProjectdescription);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert the project is created
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains masterproject", bodyText.contains(MasterProject));
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains second masterproject", bodyText.contains(SecondProject));

        //open project and add category
        String Cat1 = "Generic Category 1";
        String Cat2 = "Generic Category 2";

        driver.findElement(By.linkText(MasterProject)).click();
        driver.findElement(By.cssSelector("form > input[name=\"name\"]")).clear();
        driver.findElement(By.cssSelector("form > input[name=\"name\"]")).sendKeys(Cat1);
        driver.findElement(By.cssSelector("td.left > form > input.button")).click();
        driver.findElement(By.cssSelector("form > input[name=\"name\"]")).clear();
        driver.findElement(By.cssSelector("form > input[name=\"name\"]")).sendKeys(Cat2);
        driver.findElement(By.cssSelector("td.left > form > input.button")).click();

        //assert changes
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Categories are added properly", bodyText.contains(Cat1));
        Assert.assertTrue("Categories are added properly", bodyText.contains(Cat2));

        //open project and copy category to second project
        new Select(driver.findElement(By.name("other_project_id"))).selectByVisibleText(SecondProject);
        driver.findElement(By.name("copy_to")).click();

        //assert changes
        driver.get(URL + "manage_proj_page.php");
        driver.findElement(By.linkText(SecondProject)).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Categories are added properly", bodyText.contains(Cat1));
        Assert.assertTrue("Categories are added properly", bodyText.contains(Cat2));

        //delete categories
        driver.findElement(By.xpath("//input[@value='Delete']")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();
        driver.findElement(By.xpath("//input[@value='Delete']")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert deletion
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Categories are removed properly 1a", bodyText.contains(Cat1));

        //copy from project
        new Select(driver.findElement(By.name("other_project_id"))).selectByVisibleText(MasterProject);
        driver.findElement(By.name("copy_from")).click();

        //assert copy from
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Categories are added properly", bodyText.contains(Cat1));
        Assert.assertTrue("Categories are added properly", bodyText.contains(Cat2));

        //delete projects
        driver.get(URL + "manage_proj_page.php");
        driver.findElement(By.linkText(MasterProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText(SecondProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");
    }

    @Test //completed
    public void editProjectVersions() {
        //login as admin
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage projects screen
        driver.get(URL + "manage_proj_page.php");
        Assert.assertTrue(driver.getTitle().contains("Manage Projects - MantisBT"));

        //create project to edit
        String MasterProject = "Version Project";
        String MasterProjectdescription = "Project for editing the version of the project";
        String SecondProject = "Second Version";
        String SecondProjectDescription = "Project for editing the version of the project";

        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(MasterProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(MasterProjectdescription);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        driver.get(URL + "manage_proj_page.php");
        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(SecondProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(SecondProjectDescription);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert the project is created
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains masterproject", bodyText.contains(MasterProject));
        Assert.assertTrue("Project overview page contains second project", bodyText.contains(SecondProject));

        //open project and Add version
        driver.findElement(By.linkText(MasterProject)).click();
        String versionFirst = "0.2";
        driver.findElement(By.name("version")).clear();
        driver.findElement(By.name("version")).sendKeys(versionFirst);
        driver.findElement(By.name("add_version")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert changes
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project now contains version", bodyText.contains(versionFirst));

        //add and edit version
        String versionSecond = "2.3";
        driver.findElement(By.name("version")).clear();
        driver.findElement(By.name("version")).sendKeys(versionSecond);
        driver.findElement(By.name("add_and_edit_version")).click();
        driver.findElement(By.linkText("Proceed")).click();
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys("This is version Add and edit");
        driver.findElement(By.xpath("//input[@value='Update Version']")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert changes
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project now contains version", bodyText.contains(versionSecond));

        //copy version to second project
        new Select(driver.findElement(By.xpath("(//select[@name='other_project_id'])[2]"))).selectByVisibleText(SecondProject);
        driver.findElement(By.xpath("(//input[@name='copy_to'])[2]")).click();

        //assert in second project
        driver.get(URL + "manage_proj_page.php");
        driver.findElement(By.linkText(SecondProject)).click();
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project now contains version", bodyText.contains(versionFirst));
        Assert.assertTrue("Project now contains version", bodyText.contains(versionSecond));


        //delete version in second project
        driver.findElement(By.xpath("//input[@value='Delete']")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();
        driver.findElement(By.xpath("//input[@value='Delete']")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert deletion
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Project no longer contains version", bodyText.contains(versionFirst));
        Assert.assertFalse("Project no longer contains second version", bodyText.contains(versionSecond));

        //copy from first project
        new Select(driver.findElement(By.xpath("(//select[@name='other_project_id'])[2]"))).selectByVisibleText(MasterProject);
        driver.findElement(By.xpath("(//input[@name='copy_from'])[2]")).click();

        //assert adding versions
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project now contains version", bodyText.contains(versionFirst));
        Assert.assertTrue("Project now contains version", bodyText.contains(versionSecond));

        //delete both projects
        driver.get(URL + "manage_proj_page.php");
        driver.findElement(By.linkText(MasterProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText(SecondProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");

    }

    @Test //mostly completed
    public void editProjectAddUserToProject() {
        //login as admin
        driver.get(URL);
        driver.findElement(By.name("username")).sendKeys("Administrator");
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.cssSelector("input.button")).click();

        //go to manage projects screen
        driver.get(URL + "manage_proj_page.php");
        Assert.assertTrue(driver.getTitle().contains("Manage Projects - MantisBT"));

        //create projects to edit
        String MasterProject = "User Project";
        String MasterProjectdescription = "Project for adding and removing users";
        String copyToProject = "Copy User Project";
        String copyToProjectdescription = "(supporting) Project for adding and removing users";

        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(MasterProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(MasterProjectdescription);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        driver.findElement(By.cssSelector("td.form-title > form > input.button-small")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(copyToProject);
        driver.findElement(By.name("description")).clear();
        driver.findElement(By.name("description")).sendKeys(copyToProjectdescription);
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert the project is created
        String bodyText;
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains masterproject", bodyText.contains(MasterProject));
        Assert.assertTrue("Project overview page contains copytoproject", bodyText.contains(copyToProject));

        //todo check if all users are present: viewer, reporter, updater, developer, manager, administrator

        //open project and edit
        driver.findElement(By.linkText(MasterProject)).click();

        //open project and add users at various access levels
        //viewer, reporter, updater, developer, manager, administrator
        new Select(driver.findElement(By.name("user_id[]"))).selectByVisibleText("reporter");
        new Select(driver.findElement(By.name("access_level"))).selectByVisibleText("reporter");
        driver.findElement(By.xpath("//input[@value='Add User']")).click();
        new Select(driver.findElement(By.name("user_id[]"))).selectByVisibleText("viewer");
        new Select(driver.findElement(By.name("access_level"))).selectByVisibleText("viewer");
        driver.findElement(By.xpath("//input[@value='Add User']")).click();
        new Select(driver.findElement(By.name("user_id[]"))).selectByVisibleText("updater");
        new Select(driver.findElement(By.name("access_level"))).selectByVisibleText("updater");
        driver.findElement(By.xpath("//input[@value='Add User']")).click();
        new Select(driver.findElement(By.name("user_id[]"))).selectByVisibleText("developer");
        new Select(driver.findElement(By.name("access_level"))).selectByVisibleText("developer");
        driver.findElement(By.xpath("//input[@value='Add User']")).click();
        new Select(driver.findElement(By.name("user_id[]"))).selectByVisibleText("manager");
        new Select(driver.findElement(By.name("access_level"))).selectByVisibleText("manager");
        driver.findElement(By.xpath("//input[@value='Add User']")).click();
        new Select(driver.findElement(By.name("user_id[]"))).selectByVisibleText("TestUser1");
        new Select(driver.findElement(By.name("access_level"))).selectByVisibleText("administrator");
        driver.findElement(By.xpath("//input[@value='Add User']")).click();

        //assert changes
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains reporter", bodyText.contains("reporter test@test.com reporter"));
        Assert.assertTrue("Project overview page contains viewer", bodyText.contains("viewer test@test.com viewer"));
        Assert.assertTrue("Project overview page contains updater", bodyText.contains("updater test@test.com updater"));
        Assert.assertTrue("Project overview page contains manager", bodyText.contains("manager test@test.com manager"));
        Assert.assertTrue("Project overview page contains developer", bodyText.contains("developer test@test.com developer"));
        Assert.assertTrue("Project overview page contains administrator", bodyText.contains("TestUser1 test@test.com administrator"));

        //switch project
        //copy users from project
        driver.get(URL + "manage_proj_page.php");
        driver.findElement(By.linkText(copyToProject)).click();
        new Select(driver.findElement(By.cssSelector("td.left > select[name=\"other_project_id\"]"))).selectByVisibleText("User Project");
        driver.findElement(By.cssSelector("td.left > input[name=\"copy_from\"]")).click();

        //assert changes
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue("Project overview page contains reporter", bodyText.contains("reporter test@test.com reporter"));
        Assert.assertTrue("Project overview page contains viewer", bodyText.contains("viewer test@test.com viewer"));
        Assert.assertTrue("Project overview page contains updater", bodyText.contains("updater test@test.com updater"));
        Assert.assertTrue("Project overview page contains manager", bodyText.contains("manager test@test.com manager"));
        Assert.assertTrue("Project overview page contains developer", bodyText.contains("developer test@test.com developer"));
        Assert.assertTrue("Project overview page contains administrator", bodyText.contains("TestUser1 test@test.com administrator"));

        //delete user
        driver.findElement(By.cssSelector("td.center > form > input.button-small")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText("Proceed")).click();

        //assert changes
        bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertFalse("Project overview page no longer contains developer", bodyText.contains("developer test@test.com developer"));

        //delete projects
        driver.get(URL + "manage_proj_page.php");
        driver.findElement(By.linkText(MasterProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        driver.findElement(By.linkText(copyToProject)).click();
        driver.findElement(By.cssSelector("form > input.button")).click();
        driver.findElement(By.cssSelector("input.button")).click();

        //logout
        driver.get(URL + "logout_page.php");

    }

    @Test
    public void editProjectManageAccounts() {
        //login

        //go to project screen

        //create project to edit

        //open project and add users at various access levels

        //copy users from project (create new project to copy to)

        //assert changes

        //logout


    }


}
