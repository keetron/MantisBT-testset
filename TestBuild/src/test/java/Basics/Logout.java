package Basics;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by Koen on 5-2-2016.
 */
public class Logout {

    public static FirefoxDriver driver;

    public static void createDriver()
    {
        driver = new FirefoxDriver();
    }

    public void clickLogOut()
    {
        driver.findElement(By.linkText("Basics.Logout")).click();
    }

}