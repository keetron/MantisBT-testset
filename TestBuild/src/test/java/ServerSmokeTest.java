import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertTrue;

/**
 * Created by Koen on 2-2-2016.
 */
public class ServerSmokeTest {

    public static FirefoxDriver driver;

    @BeforeClass
    public static void createDriver()
    {
        driver = new FirefoxDriver();
    }
    @Before
    public void openWebpage()
    {
        driver.get("http://localhost:8057/mantisbt/");
    }
    @Test
    public void checkTheTitle_shouldFail()
    {
        assertTrue(driver.getTitle().equals("NOT MantisBT"));
    }
    @Test
    public void checkTheTitle_shouldPass()
    {
        assertTrue(driver.getTitle().equals("MantisBT"));
    }
    @After
    public void printThePageTitle()
    {
        System.out.println("The title of the page is: " + driver.getTitle());
    }
    @AfterClass
    public static void closeBrowser()
    {
        driver.quit();
    }
}
